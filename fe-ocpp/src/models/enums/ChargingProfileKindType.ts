export enum ChargingProfileKindType {
    Absolute,
    Recurring,
    Relative
}
