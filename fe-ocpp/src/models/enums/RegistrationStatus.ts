export enum RegistrationStatus {
    Accepted,
    Pending,
    Rejected
}
