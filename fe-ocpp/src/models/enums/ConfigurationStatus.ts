export enum ConfigurationStatus {
    Accepted,
    Rejected,
    RebootRequired,
    NotSupported
}
