export enum DiagnosticsStatus {
    Idle,
    Uploaded,
    UploadFailed,
    Uploading
}
