export enum AvailabilityStatus {
    Accepted,
    Rejected,
    Scheduled
}
