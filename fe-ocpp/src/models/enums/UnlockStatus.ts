export enum UnlockStatus {
    Unlocked,
    UnlockFailed,
    NotSupported
}
