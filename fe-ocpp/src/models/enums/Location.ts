export enum Location {
    Body,
    Cable,
    EV,
    Inlet,
    Outlet
}
