export enum AvailabilityType {
    Inoperative,
    Operative
}
