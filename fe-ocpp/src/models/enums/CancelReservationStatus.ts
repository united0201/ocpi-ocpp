export enum CancelReservationStatus {
    Accepted,
    Rejected
}
