export enum UpdateStatus {
    Accepted,
    Failed,
    NotSupported,
    VersionMismatch
}
