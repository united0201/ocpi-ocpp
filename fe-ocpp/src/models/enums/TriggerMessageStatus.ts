export enum TriggerMessageStatus {
    Accepted,
    Rejected,
    NotImplemented
}
