export enum ChargingProfileStatus {
    Accepted,
    Rejected,
    NotSupported
}
