export enum ReadingContext {
    InterruptionBegin = 'Interruption.Begin',
    InterruptionEnd = 'Interruption.End',
    Other = 'Other',
    SampleClock = 'Sample.Clock',
    SamplePeriodic = 'Sample.Periodic',
    TransactionBegin = 'Transaction.Begin',
    TransactionEnd = 'Transaction.End',
    Trigger = 'Trigger'
}
