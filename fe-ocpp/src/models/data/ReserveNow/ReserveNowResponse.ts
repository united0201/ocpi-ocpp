import { ReservationStatus } from "../../enums/ReservationStatus";

export interface ReserveNowResponse {
    status: ReservationStatus;
}
