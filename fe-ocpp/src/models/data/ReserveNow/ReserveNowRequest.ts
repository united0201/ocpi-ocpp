import { IdToken } from "../../dto/IdToken";

export interface ReserveNowRequest {
    connectorId: number;
    expiryDate: Date;
    idTag: IdToken;
    parentIdTag: IdToken;
    reservationId: number;
}
