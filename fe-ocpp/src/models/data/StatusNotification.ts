import { ChargePointErrorCode } from "../enums/ChargePointErrorCode";
import { ChargePointStatus } from "../enums/ChargePointStatus";

export interface StatusNotification {
    connectorId: number;
    errorCode: ChargePointErrorCode;
    info: string;
    status: ChargePointStatus;
    timestamp: string;
    vendorId: string;
    vendorErrorCode: string;
}
