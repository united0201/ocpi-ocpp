import { RemoteStartStopStatus } from "../../enums/RemoteStartStopStatus";

export interface RemoteStopTransactionResponse {
    status: RemoteStartStopStatus;
}
