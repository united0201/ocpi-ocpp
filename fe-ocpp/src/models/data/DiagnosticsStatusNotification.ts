import { DiagnosticsStatus } from "../enums/DiagnosticsStatus";

export interface DiagnosticsStatusNotification {
    status: DiagnosticsStatus;
}
