import { UpdateStatus } from "../../enums/UpdateStatus";

export interface SendLocalListResponse {
    status: UpdateStatus;
}
