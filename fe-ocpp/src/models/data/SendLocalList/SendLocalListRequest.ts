import { AuthorizationData } from "../../dto/AuthorizationData";
import { UpdateType } from "../../enums/UpdateType";

export interface SendLocalListRequest {
    listVersion: number;
    localAuthorizationList: AuthorizationData;
    updateType: UpdateType;
}
