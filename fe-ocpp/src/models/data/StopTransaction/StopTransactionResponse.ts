import { IdTagInfo } from "../../dto/IdTagInfo";

export interface StopTransactionResponse {
    idTagInfo: IdTagInfo;
}
