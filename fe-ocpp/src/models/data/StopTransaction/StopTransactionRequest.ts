import { IdToken } from "../../dto/IdToken";
import { Reason } from "../../enums/Reason";
import { MeterValue } from "../../dto/MeterValue";

export interface StopTransactionRequest {
    idTag: IdToken;
    meterStop: number;
    timestamp: Date;
    transactionId: number;
    reason: Reason;
    transactionData: MeterValue;
}
