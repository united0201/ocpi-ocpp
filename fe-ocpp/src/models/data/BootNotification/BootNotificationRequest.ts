export interface BootNotificationRequest {
    chargeBoxSerialNumber: string;
    chargePointModel: string;
    chargePointSerialNumber: string;
    chargePointVendor: string;
    firmwareVersion: string;
    iccid: string;
    imsi: string;
    meterSerialNumber: string;
    meterType: string;
}
