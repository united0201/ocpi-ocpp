import { RegistrationStatus } from "../../enums/RegistrationStatus";

export interface BootNotificationResponse {
    currentTime: Date;
    interval: number;
    status: RegistrationStatus;
}
