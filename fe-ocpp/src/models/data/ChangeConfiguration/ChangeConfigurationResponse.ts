import { ConfigurationStatus } from "../../enums/ConfigurationStatus";

export interface ChangeConfigurationResponse {
    status: ConfigurationStatus;
}
