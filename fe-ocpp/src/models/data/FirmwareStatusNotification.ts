import { FirmwareStatus } from "../enums/FirmwareStatus";

export interface FirmwareStatusNotification {
    status: FirmwareStatus;
}
