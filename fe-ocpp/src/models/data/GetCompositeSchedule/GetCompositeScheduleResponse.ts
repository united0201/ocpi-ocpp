export interface GetCompositeScheduleResponse {
    status: GetCompositeScheduleStatus;
    connectorId: number;
    scheduleStart: string;
    chargingSchedule: ChargingSchedule;
}
