import { ChargingRateUnitType } from "../../enums/ChargingRateUnitType";

export interface GetCompositeScheduleRequest {
    connectorId: number;
    duration: number;
    chargingRateUnit: ChargingRateUnitType;
}
