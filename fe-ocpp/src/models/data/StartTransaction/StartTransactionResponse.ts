import { IdTagInfo } from "../../dto/IdTagInfo";

export interface StartTransactionResponse {
    idTagInfo: IdTagInfo;
    transactionId: number;
}
