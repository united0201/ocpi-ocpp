import { IdToken } from "../../dto/IdToken";

export interface StartTransactionRequest {
    connectorId: number;
    idTag: IdToken;
    meterStart: number;
    reservationId: number;
    timestamp: Date;
}
