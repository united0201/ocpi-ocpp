import { ChargingProfile } from "../../dto/ChargingProfile";
import { IdToken } from "../../dto/IdToken";

export interface RemoteStartTransactionRequest {
    connectorId: number;
    idTag: IdToken;
    chargingProfile: ChargingProfile;
}
