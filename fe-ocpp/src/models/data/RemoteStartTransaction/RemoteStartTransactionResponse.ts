import { RemoteStartStopStatus } from "../../enums/RemoteStartStopStatus";

export interface RemoteStartTransactionResponse {
    status: RemoteStartStopStatus;
}
