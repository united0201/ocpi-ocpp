import { KeyValue } from "../../dto/KeyValue";

export interface GetConfigurationResponse {
    configurationKey: KeyValue;
    unknownKey: string;
}
