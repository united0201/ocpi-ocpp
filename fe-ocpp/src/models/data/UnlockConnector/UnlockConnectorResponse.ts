import { UnlockStatus } from "../../enums/UnlockStatus";

export interface UnlockConnectorResponse {
    status: UnlockStatus;
}
