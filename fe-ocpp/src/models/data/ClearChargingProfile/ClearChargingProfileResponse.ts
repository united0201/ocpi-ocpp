import { ClearChargingProfileStatus } from "../../enums/ClearChargingProfileStatus";

export interface ClearChargingProfileResponse {
    status: ClearChargingProfileStatus;
}
