import { ChargingProfilePurposeType } from "../../enums/ChargingProfilePurposeType";

export interface ClearChargingProfileRequset {
    id: number;
    connectorId: number;
    chargingProfilePurpose: ChargingProfilePurposeType;
    stackLevel: number;
}
