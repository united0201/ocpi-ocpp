import { ChargingProfileStatus } from "../../enums/ChargingProfileStatus";

export interface SetChargingProfileResponse {
    status: ChargingProfileStatus;
}
