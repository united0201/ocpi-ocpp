import { ChargingProfile } from "../../dto/ChargingProfile";

export interface SetChargingProfileRequest {
    connectorId: number;
    csChargingProfiles: ChargingProfile;
}
