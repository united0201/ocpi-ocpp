export interface UpdateFirmware {
    location: string;
    retries: number;
    retrieveDate: Date;
    retryInterval: number;
}
