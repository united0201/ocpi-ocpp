import { ResetType } from "../../enums/ResetType";

export interface ResetRequest {
    type: ResetType;
}
