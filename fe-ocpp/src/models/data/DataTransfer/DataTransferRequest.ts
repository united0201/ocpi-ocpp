export interface DataTransferRequest {
    vendorId: string;
    messageId: string;
    data: string;
}
