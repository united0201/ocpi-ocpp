import { DataTransferStatus } from "../../enums/DataTransferStatus";

export interface DataTransferResponse {
    status: DataTransferStatus;
    data: string;
}
