import { CancelReservationStatus } from "../../enums/CancelReservationStatus";

export interface CancelReservationResponse {
    status: CancelReservationStatus;
}
