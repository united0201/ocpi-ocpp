export interface CancelReservationRequest {
    reservationId: number;
}
