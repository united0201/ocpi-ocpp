import { MessageTrigger } from "../../enums/MessageTrigger";

export interface TriggerMessageRequest {
    requestedMessage: MessageTrigger;
    connectorId: number;
}
