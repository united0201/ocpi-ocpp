import { TriggerMessageStatus } from "../../enums/TriggerMessageStatus";

export interface TriggerMessageResponse {
    status: TriggerMessageStatus;
}
