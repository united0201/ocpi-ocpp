import { AvailabilityType } from "../../enums/AvailabilityType";

export interface ChangeAvailabilityRequest {
    connectorId: number;
    type: AvailabilityType;
}
