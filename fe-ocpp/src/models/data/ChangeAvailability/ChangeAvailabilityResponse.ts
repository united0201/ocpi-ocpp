import { AvailabilityStatus } from "../../enums/AvailabilityStatus";

export interface ChangeAvailabilityResponse {
    status: AvailabilityStatus;
}
