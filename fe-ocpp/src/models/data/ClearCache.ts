import { ClearCacheStatus } from "../enums/ClearCacheStatus";

export interface ClearCache {
    status: ClearCacheStatus;
}
