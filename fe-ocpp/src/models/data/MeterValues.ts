export interface MeterValues {
    connectorId: number;
    transactionId: number;
    meterValue: MeterValue;
}
