export class KeyValue {
    key: string;
    readonly: boolean;
    value: string;
}
