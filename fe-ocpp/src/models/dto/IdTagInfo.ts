import { AuthorizationStatus } from "../enums/AuthorizationStatus";
import { IdToken } from "./IdToken";

export class IdTagInfo {
    expiryDate: Date;
    parentIdTag: IdToken;
    status: AuthorizationStatus;
}
