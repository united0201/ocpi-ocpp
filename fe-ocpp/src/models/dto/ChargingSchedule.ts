import { ChargingRateUnitType } from "../enums/ChargingRateUnitType";

export class ChargingSchedule {
    duration: number;
    startSchedule: string;
    chargingRateUnit: ChargingRateUnitType;
    chargingSchedulePeriod: ChargingSchedulePeriod;
    minChargingRate: number;

}
