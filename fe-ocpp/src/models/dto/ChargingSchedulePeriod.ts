export class ChargingSchedulePeriod {
    startPeriod: number;
    limit: number;
    numberPhases: number;
}
