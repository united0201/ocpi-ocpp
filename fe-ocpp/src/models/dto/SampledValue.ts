import { ReadingContext } from "../enums/ReadingContext";
import { Measurand } from "../enums/Measurand";
import { Phase } from "../enums/Phase";
import { ValueFormat } from "../enums/ValueFormat";
import { UnitOfMeasure } from "../enums/UnitOfMeasure";
import { Location } from "../enums/Location";

export class SampledValue {
    value: string;
    context: ReadingContext;
    format: ValueFormat;
    measurand: Measurand;
    phase: Phase;
    location: Location;
    unit: UnitOfMeasure;
}
