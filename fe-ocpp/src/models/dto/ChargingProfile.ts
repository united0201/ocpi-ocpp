import { ChargingProfilePurposeType } from "../enums/ChargingProfilePurposeType";
import { ChargingProfileKindType } from "../enums/ChargingProfileKindType";
import { ChargingSchedule } from "./ChargingSchedule";
import { RecurrencyKindType } from "../enums/RecurrencyKindType";

export class ChargingProfile {
    chargingProfileId: number;
    transactionId: number;
    stackLevel: number;
    chargingProfilePurpose: ChargingProfilePurposeType;
    chargingProfileKind: ChargingProfileKindType;
    recurrencyKind: RecurrencyKindType;
    validFrom: string;
    validTo: string;
    chargingSchedule: ChargingSchedule;
}
