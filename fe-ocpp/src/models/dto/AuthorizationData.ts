import { IdTagInfo } from "./IdTagInfo";
import { IdToken } from "./IdToken";

export class AuthorizationData {
    idTag: IdToken;
    idTagInfo: IdTagInfo;
}
