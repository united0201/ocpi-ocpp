import { CdrDimensionType } from "../enums/CdrDimensionType";

export interface CdrDimension {
    type: CdrDimensionType;
    volume: number;
}
