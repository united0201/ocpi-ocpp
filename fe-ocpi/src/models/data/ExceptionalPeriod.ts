export interface ExceptionalPeriod {
    periodBegin: string;
    periodEnd: string;
}
