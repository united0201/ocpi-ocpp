import { TariffDimensionType } from "../enums/TariffDimensionType";

export interface PriceComponent {
    type: TariffDimensionType;
    price: number;
    vat: number;
    stepSize: number;
}
