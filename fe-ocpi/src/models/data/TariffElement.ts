import { PriceComponent } from "./PriceComponent";
import { TariffRestrictions } from "./TariffRestrictions";

export interface TariffElement {
    priceComponent: PriceComponent;
    restrictions: TariffRestrictions;
}
