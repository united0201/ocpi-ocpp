import { ImageCategory } from "../enums/ImageCategory";

export interface Image {
    url: string;
    thumbnail: string;
    category: ImageCategory;
    type: string;
    width:number;
    height: number;
}
