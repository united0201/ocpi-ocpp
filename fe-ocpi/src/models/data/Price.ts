export interface Price {
    exclVat: number;
    inclVat: number;
}
