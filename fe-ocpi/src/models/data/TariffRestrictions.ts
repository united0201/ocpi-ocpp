import { DayOfWeek } from "../enums/DayOfWeek";
import { ReservationRestrictionType } from "../enums/ReservationRestrictionType";

export interface TariffRestrictions {
    startTime: string;
    endTime: string;
    startDate: string;
    endDate: string;
    minKwh: number;
    maxKwh: number;
    minCurrent: number;
    maxCurrent: number;
    minPower: number;
    maxPower: number;
    minDuration: number;
    maxDuration: number;
    dayOfWeek: DayOfWeek;
    reservation: ReservationRestrictionType;
}
