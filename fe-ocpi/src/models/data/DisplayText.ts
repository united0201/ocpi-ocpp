export interface DisplayText {
    language: string;
    text: string;
}
