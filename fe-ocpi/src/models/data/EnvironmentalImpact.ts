import { EnvironmentalImpactCategory } from "../enums/EnvironmentalImpactCategory";

export interface EnvironmentalImpact {
    category: EnvironmentalImpactCategory;
    amount: number;
}
