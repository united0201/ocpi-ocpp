import { Role } from "../enums/Role";
import { BusinessDetails } from "./BusinessDetails";

export interface CredentialsRole {
    role: Role;
    businessDetails: BusinessDetails;
    partyId: string;
    countryCode: string;
}
