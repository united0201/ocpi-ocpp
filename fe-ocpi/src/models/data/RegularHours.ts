export interface RegularHours {
    weekday: number;
    periodBegin: string;
    periodEnd: string;
}
