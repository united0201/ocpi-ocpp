import { ModuleID } from "../enums/ModuleID";
import { Role } from "../enums/Role";

export interface Endpoint {
    identifier: ModuleID;
    role: Role;
    url: string;
}
