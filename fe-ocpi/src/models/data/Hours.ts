import { ExceptionalPeriod } from "./ExceptionalPeriod";
import { RegularHours } from "./RegularHours";

export interface Hours {
    regularHours: RegularHours[];
    exceptionalOpenings: ExceptionalPeriod[];
    exceptionalClosings: ExceptionalPeriod[];
}
