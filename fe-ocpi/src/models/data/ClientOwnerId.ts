export interface ClientOwnerId {
    partyId: string;
    countryCode: string;
}
