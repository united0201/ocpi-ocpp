import { ProfileType } from "../enums/ProfileType";

export interface ChargingPreferences {
    profileType: ProfileType;
    departureTime: string;
    energyNeed: number;
    dischargeAllowed: boolean;
}
