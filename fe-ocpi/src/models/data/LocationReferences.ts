export interface LocationReferences {
    locationId: string;
    evseUids: string[];
    connectorIds: string[];
}
