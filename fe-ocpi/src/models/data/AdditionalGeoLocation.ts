import { DisplayText } from "./DisplayText";

export interface AdditionalGeoLocation {
    latitude: number;
    longitude: number;
    name: DisplayText;
}
