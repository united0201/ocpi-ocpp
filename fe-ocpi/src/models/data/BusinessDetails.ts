import { Image } from "./Image";

export interface BusinessDetails {
    name: string;
    website: string;
    logo: Image;
}
