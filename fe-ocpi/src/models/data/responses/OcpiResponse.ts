export class OcpiResponse {
    data: Object;
    statusCode: number;
    statusMessage: string;
    timestamp: string;
}
