import { ChargingProfileResponseType } from "../../enums/ChargingProfileResponseType";

export interface ChargingProfileResponse {
    result: ChargingProfileResponseType;
    timeout: number;
}
