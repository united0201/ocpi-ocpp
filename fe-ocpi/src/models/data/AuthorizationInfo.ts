import { Allowed } from "../enums/Allowed";
import { LocationReferences } from "./LocationReferences";
import { DisplayText } from "./DisplayText";
import { Token } from "../dto/Token";

export interface AuthorizationInfo {
    allowed: Allowed;
    token: Token;
    location: LocationReferences;
    authorizationReference: string;
    info: DisplayText;
}
