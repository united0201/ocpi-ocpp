import { TokenType } from "../enums/TokenType";

export interface CdrToken {
    uid: string;
    tokenType: TokenType;
    contractId: string;
}
