import { EnvironmentalImpact } from "./EnvironmentalImpact";
import { EnergySource } from "./EnergySource";

export interface EnergyMix {
    supplierName: string;
    energyProductName: string;
    energySources: EnergySource[];
    environImpact: EnvironmentalImpact[];
}
