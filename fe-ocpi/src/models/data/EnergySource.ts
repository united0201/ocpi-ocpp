import { EnergySourceCategory } from "../enums/EnergySourceCategory";

export interface EnergySource {
    source: EnergySourceCategory;
    percentage: number;
}
