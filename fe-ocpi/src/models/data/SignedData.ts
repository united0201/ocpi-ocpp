import { SignedValue } from "./SignedValue";

export interface SignedData {
    encodingMethod:string;
    encodingMethodVersion: number;
    publicKey: string;
    signedValues: SignedValue[];
    url: string;
}
