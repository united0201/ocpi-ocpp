export interface PaginationAggregate {
    dtos: any[];
    totalCount: number;
}
