export interface SignedValue {
    nature: string;
    plainData: string;
    signedData: string;
}
