export interface StatusSchedule {
    periodBegin: string;
    periodEnd: string;
    status: string;
}
