export interface CdrLocation {
    id: string;
    name: string;
    address: string;
    city: string;
    postalCode: string;
    country: string;
}
