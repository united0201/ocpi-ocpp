import { CdrDimension } from "./CdrDimension";

export interface ChargingPeriod {
    startDateTime: string;
    dimensions: CdrDimension[];
    tariffId: string;
}
