import { Role } from "../../enums/Role";
import { ConnectionStatus } from "../../enums/ConnectionStatus";

export interface ClientInfoRequest {
    partyId: string;
    countryCode: string;
    role: Role;
    status: ConnectionStatus;
}
