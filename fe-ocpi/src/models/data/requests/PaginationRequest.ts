export class PaginationRequest {
    dateFrom: string;
    dateTo: string;
    offset: number = 0;
    limit: number;
}
