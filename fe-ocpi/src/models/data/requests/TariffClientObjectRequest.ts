export interface TariffClientObjectRequest {
    countryCode: string;
    partyId: string;
    tariffId: string;
}
