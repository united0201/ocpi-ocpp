import { Role } from "../enums/Role";
import { ConnectionStatus } from "../enums/ConnectionStatus";

export class ClientInfo {
    partyId: string;
    countryCode: string;
    role: Role;
    status: ConnectionStatus;
    lastUpdated: string;
}
