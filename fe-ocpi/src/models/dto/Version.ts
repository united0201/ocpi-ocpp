import { VersionNumber } from "../enums/VersionNumber";
import { Endpoint } from "../data/Endpoint";

export class Version {
    version: VersionNumber;
    endpoints: Endpoint[];
}
