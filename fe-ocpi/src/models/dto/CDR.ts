import { AuthMethod } from "../enums/AuthMethod";
import { CdrToken } from "../data/CdrToken";
import { CdrLocation } from "../data/CdrLocation";
import { SignedData } from "../data/SignedData";
import { Price } from "../data/Price";
import { ChargingPeriod } from "../data/ChargingPeriod";
import { Tariff } from "./Tariff";

export class CDR {
    countryCode: string;
    partyId: string;
    id: string;
    startDateTime: string;
    endDateTime: string;
    sessionId: string;
    cdrToken: CdrToken;
    authMethod: AuthMethod;
    authorizationReference: string;
    cdrLocation: CdrLocation;
    meterId: string;
    currency: string;
    signedData: SignedData;
    totalCost: Price;
    totalFixedCost: Price;
    totalEnergy: number;
    totalEnergyCost: Price;
    totalTime: number;
    totalTimeCost: Price;
    totalParkingTime: number;
    totalParkingCost: Price;
    totalReservationCost: Price;
    remark: string;
    credit: boolean;
    creditReferenceId: string;
    lastUpdated: string;
    tariffs: Tariff[];
    chargingPeriods: ChargingPeriod[];
}
