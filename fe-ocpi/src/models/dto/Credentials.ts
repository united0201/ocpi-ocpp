import { CredentialsRole } from "../data/CredentialsRole";

export class Credentials {
    token: string;
    url: string;
    roles: CredentialsRole[];
}
