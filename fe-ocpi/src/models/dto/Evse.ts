import { Status } from "../enums/Status";
import { StatusSchedule } from "../data/StatusSchedule";
import { Capability } from "../enums/Capability";
import { Connector } from "./Connector";
import { GeoLocation } from "../data/GeoLocation";
import { DisplayText } from "../data/DisplayText";
import { ParkingRestrictions } from "../enums/ParkingRestrictions";
import { Image } from "../data/Image";

export class Evse {
    uid: string;
    evseId: string;
    status: Status;
    statusSchedule: StatusSchedule[];
    capabilities: Capability[];
    connectors: Connector[];
    floorLevel: string;
    coordinates: GeoLocation;
    physicalReference: string;
    directions: DisplayText[];
    parkingRestrictions: ParkingRestrictions[];
    images: Image[];
    location: Location;
    lastUpdated: string;
}
