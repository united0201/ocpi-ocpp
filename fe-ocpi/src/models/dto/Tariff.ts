import { TariffType } from "../enums/TariffType";
import { DisplayText } from "../data/DisplayText";
import { Price } from "../data/Price";
import { TariffElement } from "../data/TariffElement";
import { EnergyMix } from "../data/EnergyMix";

export class Tariff {
    countryCode: string;
    partyId: string;
    id: string;
    currency: string;
    type: TariffType;
    tariffAltText: DisplayText[];
    tariffAltUrl: string;
    minPrice: Price;
    maxPrice: Price;
    elements: TariffElement[];
    startDateTime: string;
    endDateTime: string;
    energyMix: EnergyMix;
    lastUpdated: string;
}
