import { ConnectorType } from "../enums/ConnectorType";
import { ConnectorFormat } from "../enums/ConnectorFormat";
import { PowerType } from "../enums/PowerType";
import { Tariff } from "./Tariff";
import { Evse } from "./Evse";

export class Connector {
    id: string;
    standard: ConnectorType;
    format: ConnectorFormat;
    powerType: PowerType;
    maxVoltage: number;
    maxAmperage: number;
    maxElectricPower: number;
    tariffs: Tariff[];
    termsAndConditionsUrl: string;
    lastUpdated: string;
    evse: Evse;
}
