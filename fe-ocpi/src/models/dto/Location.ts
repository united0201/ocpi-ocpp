import { LocationType } from "../enums/LocationType";
import { GeoLocation } from "../data/GeoLocation";
import { AdditionalGeoLocation } from "../data/AdditionalGeoLocation";
import { Evse } from "./Evse";
import { DisplayText } from "../data/DisplayText";
import { BusinessDetails } from "../data/BusinessDetails";
import { Facility } from "../enums/Facility";
import { Hours } from "../data/Hours";
import { Image } from "../data/Image";
import { EnergyMix } from "../data/EnergyMix";

export class Location {
    id: string;
    countryCode: string;
    partyId: string;
    locationType: LocationType;
    name: string;
    address: string;
    postalCode: string;
    state: string;
    country: string;
    coordinates: GeoLocation;
    relatedLocations: AdditionalGeoLocation[];
    evses: Evse[];
    directions: DisplayText;
    operator: BusinessDetails;
    subOperator: BusinessDetails;
    owner: BusinessDetails;
    facilities: Facility[];
    timeZone: string;
    openingTimes: Hours;
    images: Image[];
    energyMix: EnergyMix;
    lastUpdated: string;
}
