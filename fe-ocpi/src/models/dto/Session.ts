import { CdrToken } from "../data/CdrToken";
import { AuthMethod } from "../enums/AuthMethod";
import { ChargingPeriod } from "../data/ChargingPeriod";
import { Price } from "../data/Price";
import { SessionStatus } from "../enums/SessionStatus";

export class Session {
    id: string;
    countryCode: string;
    partyId: string;
    startDateTime: string;
    endDateTime: string;
    kwh: number;
    cdrToken: CdrToken;
    authMethod: AuthMethod;
    authorizationReference: string;
    connectorId: string;
    meterId: string;
    currency: string;
    chargingPeriods: ChargingPeriod[];
    totalCost: Price;
    sessionStatus: SessionStatus;
    lastUpdated: string;
}
