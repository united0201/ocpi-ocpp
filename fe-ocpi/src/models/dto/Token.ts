import { TokenType } from "../enums/TokenType";
import { WhitelistType } from "../enums/WhitelistType";
import { ProfileType } from "../enums/ProfileType";
import { EnergyContract } from "../data/EnergyContract";

export class Token {
    countryCode: string;
    partyId: string;
    uid: string;
    type: TokenType;
    visualNumber: string;
    issuer: string;
    groupId: string;
    valid: boolean;
    whitelist: WhitelistType;
    language: string;
    defaultProfileType: ProfileType;
    energyContract: EnergyContract;
    lastUpdated: string;
}
