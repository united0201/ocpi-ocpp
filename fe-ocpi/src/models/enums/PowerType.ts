export enum PowerType {
    AC_1_PHASE = 'AC single phase',
    AC_3_PHASE = 'AC three phase',
    DC = 'Direct Current',
}
