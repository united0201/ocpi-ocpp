export enum TariffDimensionType {
    ENERGY,
    FLAT,
    PARKING_TIME,
    TIME
}
