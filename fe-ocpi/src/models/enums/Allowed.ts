export enum Allowed {
    ALLOWED,
    BLOCKED,
    EXPIRED,
    NO_CREDIT,
    NOT_ALLOWED
}
