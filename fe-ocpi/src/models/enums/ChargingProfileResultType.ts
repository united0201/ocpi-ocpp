export enum ChargingProfileResultType {
    ACCEPTED,
    REJECTED,
    UNKNOWN
}
