export enum WhitelistType {
    ALWAYS,
    ALLOWED,
    ALLOWED_OFFLINE,
    NEVER
}
