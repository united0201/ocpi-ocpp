export enum ReservationRestrictionType {
    RESERVATION,
    RESERVATION_EXPIRES
}
