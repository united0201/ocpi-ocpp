export enum EnvironmentalImpactCategory {
    NUCLEAR_WASTE,
    CARBON_DIOXIDE
}
