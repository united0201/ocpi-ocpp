export enum Role {
    CPO,
    EMSP,
    HUB,
    NAP,
    NSP,
    OTHER,
    SCSP
}
