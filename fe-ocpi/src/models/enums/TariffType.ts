export enum TariffType {
    AD_HOC_PAYMENT,
    PROFILE_CHEAP,
    PROFILE_FAST,
    PROFILE_GREEN,
    REGULAR
}
