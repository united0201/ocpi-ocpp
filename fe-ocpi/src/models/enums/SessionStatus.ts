export enum SessionStatus {
    ACTIVE,
    COMPLETED,
    INVALID,
    PENDING
}
