export enum LocationType {
    ON_STREET,
    PARKING_GARAGE,
    UNDERGROUND_GARAGE,
    PARKING_LOT,
    OTHER,
    UNKNOWN
}
