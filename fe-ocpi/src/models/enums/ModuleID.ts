export enum ModuleID {
    CDRS = 'cdrs',
    CHARGING_PROFILES = 'chargingprofiles',
    COMMANDS = 'commands',
    CREDENTIALS_AND_REGISTRATION = 'credentials',
    HUB_CLIENT_INFO = 'hubclientinfo',
    LOCATIONS = 'locations',
    SESSIONS = 'sessions',
    TARIFFS = 'tariffs',
    TOKENS = 'tokens',
    VERSIONS = 'versions',
}
