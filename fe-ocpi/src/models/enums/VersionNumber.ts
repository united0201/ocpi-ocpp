export enum VersionNumber {
    V2_0 = '2.0',
    V2_1 = '2.1',
    V2_1_1 = '2.1.1',
    V2_2 = '2.2'
}
