export enum ChargingProfileResponseType {
    ACCEPTED,
    NOT_SUPPORTED,
    REJECTED,
    TOO_OFTEN,
    UNKNOWN_SESSION
}
