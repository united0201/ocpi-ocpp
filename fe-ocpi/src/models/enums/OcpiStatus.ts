export const OcpiStatus: OcpiStatusEnum = {
    SUCCESS: {status: 1000, message: 'Success'},

    GENERIC_CLIENT_ERROR: {status: 2000, message: 'Generic client error'},
    INVALID_OR_MISSING_PARAMETERS: {status: 2001, message: 'Invalid or missing parameters'},
    NOT_ENOUGH_INFORMATION: {status: 2002, message: 'Not enough information'},
    UNKNOWN_LOCATION: {status: 2003, message: 'Unknown Location'},

    GENERIC_SERVER_ERROR: {status: 3000, message: 'Generic server error'},
    UNABLE_TO_USE_CLIENT_API: {status: 3001, message: 'Unable to use the client\'s API'},
    UNSUPPORTED_VERSION: {status: 3002, message: 'Unsupported version'},
    NO_MATCHING_ENDPOINTS: {status: 3003, message: 'No matching endpoints or expected endpoints missing between parties'},

    UNKNOWN_RECEIVER: {status: 4001, message: 'Unknown receiver'},
    FORWARDED_REQUEST_TIMEOUT: {status: 4002, message: 'Timeout on forwarded request'},
    CONNECTION_PROBLEM: {status: 4003, message: 'Connection problem'},
};

interface OcpiStatusInstance {
    status: number;
    message: string;
}

interface OcpiStatusEnum {
    [status: string]: OcpiStatusInstance;
}
